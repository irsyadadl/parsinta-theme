# Parsinta Theme
If you think this is like a Facebook in sublime, you're absolutely right. I didn't find while I'm searching for facebook theme in this Code editor. So I decided to make it for free.

If the title bar doesn't follow the base colors, try to add this code in your setting.json

```

"workbench.colorCustomizations": {
    "titleBar.inactiveBackground":"#252d40",
    "titleBar.activeBackground":"#252d40",
}
```

Official site
https://parsinta.com

Twitter
https://twitter.com/parsintax

Facebook
https://facebook.com/parsinta

Instagram
http://instagram.com/parsintax

Discord
https://discord.gg/HdvyuWE

Testimonials
https://www.parsinta.com/testimonials